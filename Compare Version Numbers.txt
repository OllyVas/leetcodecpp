class Solution {
public:
    int compareVersion(string s1, string s2) {
        int p1 = 0, p2 = 0;
        int v1,v2;
        while(p1<s1.size() && p2<s2.size())
        {
            v1 = next(s1,p1);
            v2 = next(s2,p2);
            if(v1>v2)
                return 1;
            if(v1<v2)
                return -1;
        }
        while(p1<s1.size())
        {
            v1=next(s1,p1);
            if(v1>0)
                return 1;
        }
        while(p2<s2.size())
        {
            v2=next(s2,p2);
            if(v2>0)
                return -1;
        }
        return 0;
    }
    
    int next(string& s, int& p)
    {
        int i =0;
        while(p<s.size() && s[p]!='.')
        {
            i=i*10+(s[p++]-'0');
        }
        p++;
        return i;
    }
    
};