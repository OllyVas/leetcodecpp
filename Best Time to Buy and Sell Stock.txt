class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if (prices.size()<1) return 0;
        int m = prices[0];
        int profit = 0;
        for(auto a: prices){
            m = min(m,a);
            profit = max(profit, a-m);
        }
        return profit;
    }
};