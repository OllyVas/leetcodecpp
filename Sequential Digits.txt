class Solution {
    
    vector<int> rez;
public:
    vector<int> sequentialDigits(int l, int h) {
        rez=vector<int>();
        for(int i =1;i<9;i++)
            gen(i,l,h);
        sort(rez.begin(),rez.end());
        return rez;
    }
    
    void gen(int d, int& l, int&h )
    {
        d = d*10 + d%10 + 1;
        if(d<=h)
        {
            if(d>=l)
               rez.push_back(d);
            if(d%10<9)
              gen(d,l,h);
        }
    }
};