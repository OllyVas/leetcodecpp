class Solution {
public:
    vector<int> pancakeSort(vector<int>& A) {
        int m = A.size();
        int i;
        vector<int> rez;
        while(m>1)
        {
            i = 0;
            while(A[i]!=m)
                i++;
        
            if(i==0)
            {
                reverse(A.begin(), A.begin()+m);
                rez.push_back(m);
            }
            else if(i!=m-1){
                reverse(A.begin(),
                   A.begin()+i+1);
                reverse(A.begin(), A.begin()+m);
                rez.push_back(i+1);
                rez.push_back(m);
            }
            m--;
        }
        return rez;
    }
};
    