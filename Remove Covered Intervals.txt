class Solution {
public:
    int removeCoveredIntervals(vector<vector<int>>& intervals) {
        sort(intervals.begin(),intervals.end());
        int count =0;
        int l =-1, r=-1;
        
        for(auto a : intervals)
        {
            if(a[0]>l && a[1]>r)
               {
                   count++;
                   l=a[0];
               }
            r=max(r,a[1]);
        }
        
        return count;
    }
};