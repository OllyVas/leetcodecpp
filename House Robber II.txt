class Solution {
public:
    int rob(vector<int>& nums) {
        if(nums.size() <1)
            return 0;
        else if(nums.size() == 1)
            return nums[0];
        else if(nums.size()==2)
            return max(nums[0], nums[1]);
        vector<int> v1(nums.begin()+1, nums.end());
        vector<int> v2(nums.begin(), nums.end()-1);

        return max(robbery(v1), robbery(v2));
    }
    
    int robbery(vector<int> nums){
        nums[1] = max(nums[0], nums[1]);
        for(int i = 2; i<nums.size(); i++)
        nums[i] = max(nums[i-1], nums[i]+nums[i-2]);
        return max(nums[nums.size()-1], nums[nums.size()-2]);
    }
};