class Solution {
public:
     static bool comp(int a, int b)
     {
         return to_string(a)+to_string(b)>to_string(b)+to_string(a);
     }
    
    string largestNumber(vector<int>& nums) {
        sort(nums.begin(),nums.end(), comp);
        string rez;
        for(auto a : nums)
            rez.append(to_string(a));
        
        return rez[0]=='0'?"0":rez;
    }    
        
};