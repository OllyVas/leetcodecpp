class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        if(matrix.size() < 1 || matrix[0].size() < 1)
            return false;
        
        int i = 0; 
        int j = 0;
        while(i+1 < matrix.size() && matrix[i+1][0] <= target)
        {
            i++;
        }
        while( j+1 <= matrix[0].size() && matrix[i][0] <= target)
        {
            if(matrix[i][j] == target)
                return true;
            j++;
        }
        return false;
    }
};